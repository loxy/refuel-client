import { Component } from '@angular/core';

@Component({
  selector: 'rfl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'refuel';
}
